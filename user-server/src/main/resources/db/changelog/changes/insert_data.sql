insert into users(id, email, user_status, role ,room, social_media, phone, first_name, last_name, password, username)
values
    ('62657586-9d98-4831-bbf0-0bca247d5d78', 'admin@gmail.com','ACTIVE', 'ADMIN',1, 'vk.com/admin', '89123456789', 'admin', 'admin','$2a$10$2MIyJH.1was2rThai9t2Huyisdw/WMtbJyAyEgkUVnfi5N2BVZIT6', 'admin'),
    ('745b8307-00b5-47c7-b8c3-80a0ba0267b5', 'kirill@gmail.com','ACTIVE', 'USER',1, 'vk.com/kirill', '89123456999', 'kirill', 'kirill','$2a$10$2MIyJH.1was2rThai9t2Huyisdw/WMtbJyAyEgkUVnfi5N2BVZIT6', 'kirill');

INSERT INTO users (user_status, role, email, room, social_media, phone, first_name, last_name, password, username)
VALUES
    ('ACTIVE', 'ADMIN', 'email2@example.com', 124, 'social_media2', '1234567892', 'Alice', 'Smith', 'password2', 'alice.smith2'),
    ('ACTIVE', 'ADMIN', 'email3@example.com', 125, 'social_media3', '1234567893', 'Bob', 'Johnson', 'password3', 'bob.johnson3'),
    ('ACTIVE', 'ADMIN', 'email4@example.com', 126, 'social_media4', '1234567894', 'Emma', 'Davis', 'password4', 'emma.davis4'),
    ('ACTIVE', 'ADMIN', 'email5@example.com', 127, 'social_media5', '1234567895', 'Michael', 'Wilson', 'password5', 'michael.wilson5'),
    ('ACTIVE', 'ADMIN', 'email6@example.com', 128, 'social_media6', '1234567896', 'Olivia', 'Brown', 'password6', 'olivia.brown6'),
    ('ACTIVE', 'ADMIN', 'email7@example.com', 129, 'social_media7', '1234567897', 'William', 'Taylor', 'password7', 'william.taylor7'),
    ('ACTIVE', 'ADMIN', 'email8@example.com', 130, 'social_media8', '1234567898', 'Sophia', 'Jones', 'password8', 'sophia.jones8'),
    ('ACTIVE', 'ADMIN', 'email9@example.com', 131, 'social_media9', '1234567899', 'James', 'Anderson', 'password9', 'james.anderson9'),
    ('ACTIVE', 'ADMIN', 'email10@example.com', 132, 'social_media10', '1234567890', 'Isabella', 'Lee', 'password10', 'isabella.lee10'),
    ('ACTIVE', 'ADMIN', 'email11@example.com', 133, 'social_media11', '1234567891', 'Benjamin', 'Clark', 'password11', 'benjamin.clark11'),
    ('ACTIVE', 'ADMIN', 'email12@example.com', 134, 'social_media12', '1234567892', 'Sophia', 'Hall', 'password12', 'sophia.hall12'),
    ('ACTIVE', 'ADMIN', 'email13@example.com', 135, 'social_media13', '1234567893', 'Oliver', 'Harris', 'password13', 'oliver.harris13'),
    ('ACTIVE', 'ADMIN', 'email14@example.com', 136, 'social_media14', '1234567894', 'Amelia', 'Martin', 'password14', 'amelia.martin14'),
    ('ACTIVE', 'ADMIN', 'email15@example.com', 137, 'social_media15', '1234567895', 'Lucas', 'Moore', 'password15', 'lucas.moore15'),
    ('ACTIVE', 'ADMIN', 'email16@example.com', 138, 'social_media16', '1234567896', 'Mia', 'Jackson', 'password16', 'mia.jackson16'),
    ('ACTIVE', 'ADMIN', 'email17@example.com', 139, 'social_media17', '1234567897', 'Henry', 'White', 'password17', 'henry.white17'),
    ('ACTIVE', 'ADMIN', 'email18@example.com', 140, 'social_media18', '1234567898', 'Evelyn', 'Turner', 'password18', 'evelyn.turner18'),
    ('ACTIVE', 'ADMIN', 'email19@example.com', 141, 'social_media19', '1234567899', 'Alexander', 'Scott', 'password19', 'alexander.scott19'),
    ('ACTIVE', 'ADMIN', 'email20@example.com', 142, 'social_media20', '1234567890', 'Charlotte', 'Walker', 'password20', 'charlotte.walker20'),
    ('ACTIVE', 'ADMIN', 'email21@example.com', 143, 'social_media21', '1234567891', 'Daniel', 'Wright', 'password21', 'daniel.wright21'),
    ('ACTIVE', 'ADMIN', 'email22@example.com', 144, 'social_media22', '1234567892', 'Scarlett', 'Robinson', 'password22', 'scarlett.robinson22'),
    ('ACTIVE', 'ADMIN', 'email23@example.com', 145, 'social_media23', '1234567893', 'Matthew', 'Cooper', 'password23', 'matthew.cooper23'),
    ('ACTIVE', 'ADMIN', 'email24@example.com', 146, 'social_media24', '1234567894', 'Ava', 'King', 'password24', 'ava.king24'),
    ('ACTIVE', 'ADMIN', 'email25@example.com', 147, 'social_media25', '1234567895', 'David', 'Adams', 'password25', 'david.adams25'),
    ('ACTIVE', 'ADMIN', 'email26@example.com', 148, 'social_media26', '1234567896', 'Grace', 'Parker', 'password26', 'grace.parker26'),
    ('ACTIVE', 'ADMIN', 'email27@example.com', 149, 'social_media27', '1234567897', 'Joseph', 'Evans', 'password27', 'joseph.evans27'),
    ('ACTIVE', 'ADMIN', 'email28@example.com', 150, 'social_media28', '1234567898', 'Lily', 'Morris', 'password28', 'lily.morris28'),
    ('ACTIVE', 'ADMIN', 'email29@example.com', 151, 'social_media29', '1234567899', 'Daniel', 'Bell', 'password29', 'daniel.bell29'),
    ('ACTIVE', 'ADMIN', 'email30@example.com', 152, 'social_media30', '1234567890', 'Emily', 'Cook', 'password30', 'emily.cook30');